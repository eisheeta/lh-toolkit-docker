## Docker image for [LibreHealth Toolkit](https://librehealth.io/projects/lh-toolkit/)
[![pipeline status](https://gitlab.com/librehealth/toolkit/lh-toolkit-docker/badges/master/pipeline.svg)](https://gitlab.com/librehealth/toolkit/lh-toolkit-docker/commits/master)


The repository hosts docker compose and resource files required to build docker
images and start containers that can be used by users and developers to test the
latest code in the repository. The Docker container that is created uses standard
tomcat7:jre8 and mysql:5.7 images. It includes the following:
 - the legacy-ui module
 - webservices.rest module
 - the owa module
 - demo database for Toolkit 2.1 SNAPSHOT

The tomcat:7-jre8 uses OpenJDK 8, which in turn uses Debian 8.1 (Jessie) base
image. Thus the lh-toolkit is deployed on tomcat7. The MySQL 5.7 image is used
for the database on which the 2.1 SNAPSHOT demo database is deployed. The main
advantage of using standard images is that the updates to those are automatically
available. The lh-toolkit.war is downloaded from the latest devbuilds that are
uploaded to bintray, using Gitlab CI. Thus, the webapplication built from the
master branch is available as part of this container. You only have to update
the image and get the latest and greatest lh-toolkit that is available from the
Continuous Integration.

## Running the container
#### Requirements
Please install [Docker](https://docs.docker.com/) and
[Docker Compose](https://docs.docker.com/compose/install/) to create the
container images. You will need to pull the container image from the Gitlab Docker
Container Registry. Ensure that you do
not have any other servers running on 8080 (tomcat) and 3306 (mysql).

### Pulling the container image
To run containers pull the container image:
```
docker pull registry.gitlab.com/librehealth/toolkit/lh-toolkit-docker:latest
```
You'll get a similar output on your command prompt
![screenshot](./screenshot/step1.PNG) 

Navigate to the repository where you have cloned the project, and run it in the foreground or background as per your need.

To run the container in the foreground:
```
docker-compose -f docker-compose.dev.yml up
```
![screenshot](./screenshot/step2.PNG) 

MySQL will be started first and then lh-toolkit will be started on the containers.
When you are done using lh-toolkit you can press `Ctrl+C` to stop the container.

To run the container in the background:
```
docker-compose -f docker-compose.dev.yml up -d
```
![screenshot](./screenshot/bgcont.PNG)
![screenshot](./screenshot/bgcont2.PNG) 

## Using lh-toolkit
To start using lh-toolkit, point your browser to localhost:8080/lh-toolkit.
Enter the following authentication information on the log in form:

* **User**: admin
* **Pass**: Admin123
![screenshot](./screenshot/login_screen.png)

## Additional Notes
If you're using Docker Toolbox, point your browser to http://machineIP:8080/lh-toolkit. In place of machineIP in the address, write your own docker machine IP.
To find your machine IP:
```
docker-machine ip
```
![screenshot](./screenshot/machine_ip.PNG) 

## Errors
If you encounter the following error while running the container, make sure you're running the command in your project directory.

![screenshot](./screenshot/error.png)
